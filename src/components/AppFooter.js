
import {
  MDBFooter,
  MDBContainer,
  MDBCol,
  MDBRow,
  MDBIcon,
  MDBBtn
} from 'mdb-react-ui-kit';


export default function AppFooter() {
	return(
			<MDBFooter id="footer" className='text-center'>
			     <MDBContainer className='p-4 pb-0'>
			       <section className='mb-2'>
			         <MDBBtn id="icons"  outline color="dark" floating  href="mailto:bacudoivy@gmail.com" role='button'>
			          <MDBIcon far icon="envelope" /> 
			         </MDBBtn>

			         <MDBBtn id="icons" outline color="dark" floating  href="https://www.linkedin.com/in/ivy-bacudo" target="_blank" role='button'>
			           <MDBIcon fab icon='linkedin-in' />
			         </MDBBtn>

			         <MDBBtn id="icons" outline color="dark" floating href="https://github.com/ivybacudo" target="_blank" role='button'>
			           <MDBIcon fab icon='github' />
			         </MDBBtn>
			       </section>
			     </MDBContainer>


			     <div className='text-center p-3'>
			       © 2022 Copyright: Ivy Bacudo
			     </div>
			   </MDBFooter>

		)
}