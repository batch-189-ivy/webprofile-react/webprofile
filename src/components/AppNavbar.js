
import { MDBIcon } from 'mdb-react-ui-kit';
import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav, Container, Button } from 'react-bootstrap';




export default function AppNavbar() {

    

    return (

        <Navbar sticky="top" id="nav-bg" expand="lg">
              <Container>
                <Navbar.Brand id="nav-logo" as={Link} to="/"><MDBIcon fas icon="address-card" /> Ivy Bacudo</Navbar.Brand>
                <Navbar.Toggle id="navbar-toggle"  aria-controls="basic-navbar-nav"> <MDBIcon fas icon="hamburger" size='1x' />  </Navbar.Toggle> 
                <Navbar.Collapse >
                
                  
                  <Nav className="text-center" id="nav-right">
                    <Nav.Link id="navbar-name" as={Link} to="/" ><MDBIcon fas icon="home" /> Home</Nav.Link>
                    <Nav.Link id="navbar-name" as={Link} to="/services" ><MDBIcon fas icon="desktop" id="icon-nav"/> Services</Nav.Link>
                    <Nav.Link id="navbar-name" as={Link} to="/projects"><MDBIcon fas icon="list-alt" /> Projects</Nav.Link>
                    <Nav.Link id="navbar-icons" href="mailto:bacudoivy@gmail.com"><MDBIcon fas icon="envelope"/></Nav.Link>
                    <Nav.Link id="navbar-icons" href="https://www.linkedin.com/in/ivy-bacudo" target="_blank"><MDBIcon fab icon='linkedin-in' /></Nav.Link>
            
                  </Nav>
                </Navbar.Collapse>
              </Container>
            </Navbar>
        );

}

