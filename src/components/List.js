import {useState} from 'react';
import { MDBIcon } from 'mdb-react-ui-kit';
import { SiMongodb, SiHeroku, SiVercel, SiPostman, SiSublimetext  } from 'react-icons/si';
import {Row, Col, Container, Button, } from 'react-bootstrap'; 


export default function List() {

	
	
	return(

	<Row style={{justifyContent: "center", paddingTop: "80px", paddingBottom: "50px"}}>
		
				<h1 data-aos="zoom-out" id="list-title" className="text-center">Core Tools:</h1>

				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" >
					<MDBIcon fab icon="js" />
				</Col>


				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" >
					<MDBIcon fab icon="node"/>
				</Col>


				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" > 
					<MDBIcon fab icon="react" />
				</Col>


				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" >
					<MDBIcon fab icon="html5" />
				</Col>


				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" >
					<MDBIcon fab icon="css3-alt" />
				</Col>


				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" >
					<MDBIcon fab icon="bootstrap" />
				</Col>


				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" > 
					<MDBIcon fab icon="git-square"/>
				</Col>


				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" >
					<MDBIcon fab icon="gitlab" />
				</Col>


				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" >
					<MDBIcon fab icon="github-alt"/>
				</Col>


				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" > 
					<MDBIcon fab icon="npm" />
				</Col>


				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" >
					<SiMongodb />
				</Col>


				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" >
					<SiHeroku />
				</Col>


				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" >
					<SiVercel />
				</Col>


				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" >
					<SiPostman />
				</Col>


				<Col data-aos="zoom-out" xs={5} md={3} lg={2} id="list-icon" >
					<SiSublimetext />
				</Col>

	</Row>

	);
}
