import pic from '../images/pic.jpg';
import {Row, Col, Container} from 'react-bootstrap';
import {BsArrowUpRight} from 'react-icons/bs';
import {GrLocation} from 'react-icons/gr';
import resume from '../images/resume.png'; 


export default function Greetings() {
	return(
	<Container 

		data-aos="fade-down"
		data-aos-offset="200"
		data-aos-delay="50"
		data-aos-duration="1000"
		data-aos-easing="ease-in-out"
		data-aos-mirror="true"
		data-aos-once="false"
		data-aos-anchor-placement="top-center"

	 id="landing-page" className="border border-dark p-5">
		<Row className="">
				<Col xs={8} md={7} lg={8} className="">
					<div id="d-greet"><GrLocation/>Manila, Philippines</div>
					<div id="greet" className="text-justify">Hello,</div>
					<div id="greet" className="pb-3 text-justify">I'm Ivy Bacudo</div>
					<p id="p-greet">I am a dynamic Full Stack Software Developer based in the Philippines.</p>
					<a id="icon-greet" href="https://www.linkedin.com/in/ivy-bacudo" target="_blank"><span>Say hello on LinkedIn<BsArrowUpRight/></span></a>
					
				</Col>
					
				<Col xs={12} md={5} lg={4} >
				 <img id="greet-pic" width={350} className="rounded-circle img-fluid" src={pic}/>
				</Col>
				    
		</Row>
	</Container>

	);
}


