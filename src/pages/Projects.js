
import {Card, CardGroup, Button} from 'react-bootstrap';
import { MDBIcon } from 'mdb-react-ui-kit';
import { SiVercel } from 'react-icons/si';
import cap1 from '../images/cap1new.PNG'
import cap2 from '../images/cap2.PNG'
import cap3 from '../images/cap3.PNG'
export default function Projects() {

	return(
		<>
		<h1 id="projects-title" className="text-center">Projects</h1>
		<CardGroup id="card-container">
		      <Card data-aos="fade-right"  id="card-body" className="m-2 rounded">
		        <Card.Img className="img-fluid"  variant="top" src={cap1} />
		        <Card.Body>
		          <Card.Title>Capstone 1</Card.Title>
		          <Card.Text>
		            Responsive web design using HTML, CSS and Bootstrap.
		          </Card.Text>
		        </Card.Body>
		        
		          <Button id="card-btn" href="https://ivybacudo.github.io/webportfolio/" target="_blank"><MDBIcon fab icon="github-alt"/></Button>
		        
		      </Card>
		      <Card data-aos="zoom-in" id="card-body" className="m-2 rounded">
		        <Card.Img className="img-fluid" variant="top" src={cap2} />
		        <Card.Body>
		          <Card.Title>Capstone 2</Card.Title>
		          <Card.Text>
		            Backend E-commerce design using Express.js/NodeJS and MongoDB.{' '}
		          </Card.Text>
		        </Card.Body>
		        <Button id="card-btn" href="https://gitlab.com/batch-189-ivy/s42-s46-capstone2/capstone-2" target="_blank"><MDBIcon fab icon="gitlab" /></Button>
		      </Card>
		      <Card data-aos="fade-left" id="card-body" className="m-2 rounded">
		        <Card.Img className="img-fluid"variant="top" src={cap3} />
		        <Card.Body>
		          <Card.Title>Capstone 3</Card.Title>
		          <Card.Text>
		            E-commerce design using React JS.
		          </Card.Text>
		        </Card.Body>
		        <Button id="card-btn" href="https://capstone-3-bacudo-ecommerce-app.vercel.app/" target="_blank"><SiVercel/></Button>
		      </Card>
		    </CardGroup>
		    </>
		  );

		
}