
import {Row, Col, Container} from 'react-bootstrap';
import back from '../images/back.png'
import front from '../images/front.png'
import shop from '../images/shop.png'

export default function Services() {

	return(
	<>
		<h1 id="services-title" className="p-5 text-center">Services</h1>
		<Container id="services-container" className="container-fluid pb-5">
			

			<Row className="d-flex justify-content-center align-items-center">
				<Col data-aos="fade-right" md={5} lg={5} className="">
					<img id="front-pic" className="img-fluid" width={400} src={front}/>
				</Col>
				<Col data-aos="fade-left" md={5} lg={5} className="p-5 m-1 align-items-center">
					<h3 id="front">Front End Development</h3>
					<p>Convert your mockups into a functional and working website. Develop a responsive website layout that automatically adjusts to any device screen size.</p>
					<span id="html">HTML5</span>
					<span id="css">CSS3</span>
					<span id="bootstrap">Bootstrap</span>
				</Col>
			</Row>


			<Row className="d-flex justify-content-center align-items-center">
							<Col data-aos="fade-right" md={5} lg={5} >
								<img id="front-pic" className="img-fluid" width={400} src={back}/>
							</Col>

							<Col data-aos="fade-left" md={5} lg={5} className="p-5 m-1 align-items-center">
								<h3 id="front">Back End Development</h3>
								<p>Work on server-side development using REST API that will be tested and deployed in a database.</p>
								<span id="mongo">MongoDB</span>
								<span id="postman">Postman</span>
								<div id="div-services"></div>
								<span id="node">NodeJS</span>
								<span id="express">Express</span>
							</Col>

							
						</Row>

			<Row className="d-flex justify-content-center align-items-center">

							<Col data-aos="fade-right" md={5} lg={5} className="">
								<img id="front-pic" className="img-fluid" width={400} src={shop}/>
							</Col>

							<Col data-aos="fade-left" md={5} lg={5} className="p-5 m-1 align-items-center">
								<h3 id="front">E-commerce Website</h3>
								<p>Develop an e-commerce website using Reactjs.</p>
								<span id="react">ReactJS</span>
								<span id="nodejs">NodeJS</span>
								<div id="npm">Npm packages</div>
							</Col>

						</Row>

		</Container>
	</>
		)
}
