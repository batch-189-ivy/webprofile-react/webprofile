import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom';
import AppFooter from './components/AppFooter';
import AppNavbar from './components/AppNavbar';

import React from 'react';

import Home from './pages/Home';
import Projects from './pages/Projects';
import Services from './pages/Services';
import './App.css';


function App() {
  return (
    <Router> 
        <AppNavbar/>
        <Container>
          <Routes> 
              <Route path="/" element={<Home/>} />
              <Route path="/services" element={<Services/>} />
              <Route path="/projects" element={<Projects/>} />
          </Routes>
        </Container>
        <AppFooter/>
    </Router>
  );
}

export default App;
